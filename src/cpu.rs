const MEMORY_OFFSET: usize = 512;
const WIDTH: usize = 64;
const HEIGHT: usize = 32;

const CHIP8_FONT_SET: [u8; 80] = [
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80, // F
];

//  0E0 - CLS ++
// 00EE - RET
// 0nnn - SYS addr
// 1nnn - JP addr
// 2nnn - CALL addr
// 3xkk - SE Vx, byte
// 4xkk - SNE Vx, byte
// 5xy0 - SE Vx, Vy
// 6xkk - LD Vx, byte
// 7xkk - ADD Vx, byte
// 8xy0 - LD Vx, Vy
// 8xy1 - OR Vx, Vy
// 8xy2 - AND Vx, Vy
// 8xy3 - XOR Vx, Vy
// 8xy4 - ADD Vx, Vy
// 8xy5 - SUB Vx, Vy
// 8xy6 - SHR Vx {, Vy}
// 8xy7 - SUBN Vx, Vy
// 8xyE - SHL Vx {, Vy}
// 9xy0 - SNE Vx, Vy
// Annn - LD I, addr
// Bnnn - JP V0, addr
// Cxkk - RND Vx, byte
// Dxyn - DRW Vx, Vy, nibble
// Ex9E - SKP Vx
// ExA1 - SKNP Vx
// Fx07 - LD Vx, DT
// Fx0A - LD Vx, K
// Fx15 - LD DT, Vx
// Fx18 - LD ST, Vx
// Fx1E - ADD I, Vx
// Fx29 - LD F, Vx
// Fx33 - LD B, Vx
// Fx55 - LD [I], Vx
// Fx65 - LD Vx, [I]

pub struct Cpu {
    _i: u16,
    _pc: u16,
    _delay_timer: u8,
    _sound_timer: u8,
    _memory: [u8; 4096],
    _v: [u8; 16],
    _sp: u8,
    _stack: [u16; 16],
    _gfx: [u8; 64 * 32],
    _keys: [bool; 16],
}

impl Cpu {
    pub fn initialize() -> Cpu {
        let mut cpu = Cpu {
            _i: 0,
            _pc: 0x200,
            _sp: 0,
            _delay_timer: 0,
            _sound_timer: 0,
            _memory: [0; 4096],
            _v: [0; 16],
            _stack: [0; 16],
            _gfx: [0; WIDTH * HEIGHT],
            _keys: [false; 16],
        };

        cpu.load_fonts();

        cpu
    }

    fn load_fonts(&mut self) {
        self._memory[..80].clone_from_slice(&CHIP8_FONT_SET[..80]);
    }

    pub fn load_program(&mut self, data: Vec<u8>) {
        self._memory[MEMORY_OFFSET..].clone_from_slice(data.as_slice());
    }

    pub fn cycle(&mut self) {
        // Fetch Opcode
        let opcode = self.read_word();
        // Decode Opcode
        self.decode_opcode(opcode);
        // TODO: Execute Opcode

        // TODO: Update timers
    }

    fn decode_opcode(&mut self, opcode: u16) {
        // TODO: decode all opcodes

        let x = ((opcode & 0x0F00) >> 8) as usize;
        let y = ((opcode & 0x00F0) >> 4) as usize;
        let vx = self._v[x];
        let vy = self._v[y];
        let kk = (opcode & 0x00FF) as u8;
        let nnn = opcode & 0x0FFF;

        self._pc += 2;
        match opcode {
            0x00E0 => {
                self._gfx = [0; 64 * 32];
            }
            0x00EE => {
                self._pc = self._stack[self._sp as usize];
                // TODO: should pc be increased by 2?
                self._stack[self._sp as usize] = 0x0000;

                self._sp -= 1;
            }
            0x1000..=0x1FFF => {
                self._pc = opcode & 0x0FFF;
            }
            0x2000..=0x2FFF => {
                self._sp += 1;
                self._stack[self._sp as usize] = self._pc;

                self._pc = opcode & 0x0FFF;
            }
            0x3000..=0x3FFF => self._pc += if vx == kk as u8 { 2 } else { 0 },
            0x4000..=0x4FFF => self._pc += if vx != kk as u8 { 2 } else { 0 },
            0x5000..=0x5FFF => self._pc += if vx == vy { 2 } else { 0 },
            0x6000..=0x6FFF => self._v[x as usize] = kk,
            0x7000..=0x7FFF => self._v[x as usize] = vx + kk,
            0x8000..=0x8FFF => match opcode & 0x000F {
                0 => self._v[x] = vy,
                1 => self._v[x] = vx | vy,
                2 => self._v[x] = vx & vy,
                3 => self._v[x] = vx ^ vy,
                4 => {
                    let (value, did_overflow) = vx.overflowing_add(vy);
                    if did_overflow {
                        self._v[0xF] = 1
                    };
                    self._v[x] = value;
                }
                5 => {
                    let (value, _) = vx.overflowing_sub(vy);
                    self._v[0xF] = if vx > vy { 1 } else { 0 };
                    self._v[x] = value;
                }
                6 => {
                    self._v[0xF] = self._v[x] & 0x1;
                    self._v[x] >>= 1;
                }
                7 => {
                    let (value, did_overflow) = vx.overflowing_sub(vy);
                    self._v[0xF] = if did_overflow { 0 } else { 1 };
                    self._v[x] = value;
                }
                0xE => {
                    self._v[0xF] = vx & 0x80;
                    self._v[x] <<= 1;
                }
                _ => {}
            },
            0x9000..=0x9FFF => self._pc += if vx != vy { 2 } else { 0 },
            0xA000..=0xAFFF => self._i = nnn,
            0xB000..=0xBFFF => self._pc = nnn + self._v[0] as u16,
            0xC000..=0xCFFF => self._v[x] = rand::random::<u8>() & kk,
            0xD000..=0xDFFF => {
                // TODO: display implementation
            }
            0xE000..=0xEFFF => match opcode & 0x00FF {
                0x9E => self._pc += if self._keys[vx as usize] { 2 } else { 0 },
                0xA1 => self._pc += if self._keys[vx as usize] { 0 } else { 2 },
                _ => {}
            },
            0xF000..=0xFFFF => {}
            _ => {}
        }
    }

    fn read_word(&self) -> u16 {
        let left: u16 = self._memory[self._pc as usize] as u16;
        let right: u16 = self._memory[self._pc as usize + 1] as u16;
        left << 8 | right
    }
}
